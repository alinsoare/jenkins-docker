#!/usr/bin/env bash

function create_node_config() {
    mkdir ${1}
    cat > ${1}/config.xml << EOF
<?xml version='1.1' encoding='UTF-8'?>
<slave>
  <name>lx-$(hostname)</name>
  <description></description>
  <remoteFS>/home/jenkins</remoteFS>
  <numExecutors>2</numExecutors>
  <mode>NORMAL</mode>
  <retentionStrategy class="hudson.slaves.RetentionStrategy$Always"/>
  <launcher class="hudson.slaves.JNLPLauncher">
    <workDirSettings>
      <disabled>false</disabled>
      <internalDir>remoting</internalDir>
      <failIfWorkDirIsMissing>false</failIfWorkDirIsMissing>
    </workDirSettings>
  </launcher>
  <label>node4android</label>
  <nodeProperties/>
</slave>
EOF
}

while ! [ -d /var/jenkins_nodes ]; do sleep 5;done
NODE_DIR="/var/jenkins_nodes/lx-$(hostname)"

# exit if node dir exists
[ -d ${NODE_DIR} ] || create_node_config ${NODE_DIR}

while ! nc -z jenkins_master ${JENKINS_PORT}; do sleep 10;done
NODESECRET=$(curl -sf -u admin:${JENKINS_PASS} http://jenkins_master:${JENKINS_PORT}/computer/lx-$(hostname)/slave-agent.jnlp | xmllint --xpath "string(//jnlp/application-desc/argument[1])" -)

curl -sf http://jenkins_master:${JENKINS_PORT}/jnlpJars/agent.jar -o agent.jar && \
java -jar agent.jar -jnlpUrl http://jenkins_master:${JENKINS_PORT}/computer/lx-$(hostname)/slave-agent.jnlp -secret ${NODESECRET} -workDir /home/jenkins
