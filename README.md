# Jenkins docker environment


### Docker compose

``` bash
# build
docker-compose build
# run
docker-compose up --detach --scale jenkins-node-lx=[nodes_nr]
# remove all traces
docker system prune --all --force
```

#### Admin web interface 
> http://localhost:8080

* user: *admin*
* password: *Softvision10*

### Note:
Remove jenkins offline nodes from web interface.

### Jenkins job integration
#### Step 1:
Install plugins:

* workflow-multibranch [required]
* git [required]
* blueocean [optional]
* create credentials to get git repo access 

#### Step 2:
Create jenkins job:

* create a multibranch job/item:
* add url into Git -> Project Repository
* select credentials from first step
* select Build Configuration -> Mode; "by Jenkinsfile"
    * add Jenkinsfile script path into Build Configuration [ie: *WebBasicSample/Jenkinsfile*]
* add  Pipeline Trigger scan interval [optional]
